import spacy
import sys      
import subprocess
from collections import Counter

nlp = spacy.load('es')      #someday: make it autodetect language and load the appropriate models

# add error if no arguments are provided

filenames = sys.argv[1:]    #all arguments except the first (which is the script's path)
print(filenames)     

# *.md works here, unlike with bookbinder!

cat = 'cat'
for item in filenames:
    cat = cat+' '+item
    
#rawText = subprocess.check_output(["cat", ' '.join(filenames)]) #not working, don't know why
rawText = subprocess.check_output(cat, shell=True).decode(sys.stdout.encoding)

print(rawText)

doc = nlp(rawText)
wordCount = Counter()
goWordCount = Counter()     #go word: opposite of stop word


for token in doc:
    if token.pos_ not in (u'PUNCT', u'SPACE'):
        wordCount[token.orth_] += 1 # Equivalently, token.text
        if not token.is_stop:
            goWordCount[token.orth_] += 1

print('There are %s words in total.' % sum(wordCount.values()))
print('')
#remove stop words before the frequency list
print 'The most common are:'
for letter, count in goWordCount.most_common(10):
        print '%s %7d' % (letter, count)

#Now find out if the repeated words are very close to each other.
